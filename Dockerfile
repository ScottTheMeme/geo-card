FROM registry.gitlab.com/scottthememe/elixir-docker:1.14 as builder

WORKDIR /app

ENV MIX_ENV=prod

RUN mix local.hex --force && mix local.rebar --force

COPY mix.exs mix.lock VERSION ./
COPY config config
COPY assets assets
COPY priv priv
COPY lib lib

RUN mix build.deploy
RUN mix release geo_card

FROM alpine:latest

RUN apk add --update --no-cache openssl ncurses-libs libstdc++

WORKDIR /app
ENV HOME=/app
ENV MIX_ENV=prod

RUN chown 1234:1234 /app
USER 1234:1234

COPY --from=builder --chown=1234:1234 /app/dist/ ./

CMD ["bin/geo_card", "start"]

defmodule GeoCard.Repo.Migrations.CreateLocation do
  use Ecto.Migration

  def change do
    create table(:location) do
      add :hashed_ip, :string
      add :city, :string
      add :region, :string
      add :country_name, :string
      add :lat, :decimal
      add :long, :decimal

      timestamps()
    end
  end
end

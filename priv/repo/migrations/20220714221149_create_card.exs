defmodule GeoCard.Repo.Migrations.CreateCard do
  use Ecto.Migration

  def change do
    create table(:card) do
      add :activated, :boolean
      add :serial, :string
      add :card_secret, :string

      timestamps()
    end
  end
end

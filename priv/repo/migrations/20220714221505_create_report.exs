defmodule GeoCard.Repo.Migrations.CreateReport do
  use Ecto.Migration

  def change do
    create table(:report) do
      add :card_id, references(:card)
      add :location_id, references(:location)

      timestamps()
    end
  end
end

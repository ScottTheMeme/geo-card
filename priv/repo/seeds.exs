# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     GeoCard.Repo.insert!(%GeoCard.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
alias GeoCard.{Command, Utils, Repo}

IO.inspect(Application.get_all_env(:geocard))

card_params = %{
  activated: true,
  serial: Utils.random_string(8),
  card_secret: Utils.random_string(32)
}

card =
  %Repo.Card{}
  |> Repo.Card.changeset(card_params)
  |> Repo.insert!()

example_ips = [
  "1.1.1.1",
  "8.8.8.8",
  "9.9.9.9",
  "208.67.222.222",
  "185.228.168.9",
  "76.76.19.19"
  # "94.140.14.14"
  # europe ips dont report lat long on ipapi.co
  # possibility to use ipgeolocation.io with free plan
]

output =
  for x <- example_ips do
    Command.LocationCache.run(%{ip: x})
  end

for x <- output do
  %Repo.Report{}
  |> Repo.Report.changeset(%{card_id: card.id, location_id: x.data.location.id})
  |> Repo.insert!()
end

# output2 =
#  for x <- 1..200 do
#    Command.LocationCache.run(%{ip: Enum.random(example_ips)})
#  end
#
# for x <- output2 do
#  %Repo.Report{}
#  |> Repo.Report.changeset(%{card_id: card.id, location_id: x.data.location.id})
#  |> Repo.insert!()
# end

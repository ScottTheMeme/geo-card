defmodule GeoCard.Utils do
  @moduledoc """
  Extra utilites used in the project.
  """

  require IP

  def random_string(length) do
    :crypto.strong_rand_bytes(length) |> Base.url_encode64() |> binary_part(0, length)
  end

  def fetch_ip(%Plug.Conn{req_headers: headers} = conn) do
    bit_string_ip = RemoteIp.from(headers)

    cond do
      bit_string_ip == nil ->
        parse_inet(conn.remote_ip)

      IP.is_ipv4(bit_string_ip) or IP.is_ipv6(bit_string_ip) ->
        parse_inet(bit_string_ip)
    end
  end

  def format_date(datetime) do
    "#{datetime.month}/#{datetime.day}/#{datetime.year} #{datetime.hour}:#{datetime.minute} UTC"
  end

  defp parse_inet(ip) do
    IP.to_string(ip)
  end
end

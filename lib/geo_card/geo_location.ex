defmodule GeoCard.GeoLocation do
  @moduledoc """
  Fetches the GeoLocation of an IP from ipapi.co
  """

  use Tesla

  plug Tesla.Middleware.JSON

  defp api_key, do: Application.get_env(:geo_card, :ipgeolocation_api_key)

  def location(ip) do
    if is_nil(api_key()) do
      ipapi(ip)
    else
      ipgeolocation(ip)
    end
  end

  def ipgeolocation(ip) do
    {:ok, resp} = get("https://api.ipgeolocation.io/ipgeo?apiKey=#{api_key()}&ip=#{ip}")

    %{
      city: resp.body["city"],
      country_name: resp.body["country_name"],
      region: get_region(resp.body),
      lat: resp.body["latitude"],
      long: resp.body["longitude"]
    }
  end

  def ipapi(ip) do
    {:ok, resp} = get("https://ipapi.co/#{ip}/json")

    %{
      city: resp.body["city"],
      country_name: resp.body["country_name"],
      region: resp.body["region"],
      lat: resp.body["latitude"],
      long: resp.body["longitude"]
    }
  end

  defp get_region(body) do
    cond do
      body["district"] != "" -> body["district"]
      body["state_prov"] != "" -> body["state_prov"]
      true -> body["continent_name"]
    end
  end
end

defmodule GeoCard.CardLoader do
  @moduledoc """
  Load relevant information for a card.
  """

  alias GeoCard.Repo
  alias Repo.Card
  import Ecto.Query
  import GeoCard.Utils

  def load(serial, secret) do
    Card
    |> where(serial: ^serial, card_secret: ^secret)
    |> Repo.one()
    |> case do
      nil -> %{error: :not_found}
      card -> card
    end
  end

  def load(id) do
    Card
    |> where(id: ^id)
    |> preload(reports: :location)
    |> Repo.one()
    |> case do
      nil -> %{error: :not_found}
      card -> format(card)
    end
  end

  def format(%{reports: []}), do: %{error: :unused}

  def format(card) do
    reports =
      for x <- card.reports do
        %{
          id: x.id,
          city: x.location.city,
          country_name: x.location.country_name,
          region: x.location.region,
          date_time: format_date(x.location.inserted_at),
          lat: from_dec(x.location.lat),
          long: from_dec(x.location.long)
        }
      end

    last_report = Map.get(List.last(card.reports), :location)

    %{
      id: card.id,
      last: %{
        lat: from_dec(last_report.lat),
        long: from_dec(last_report.long)
      },
      reports: Enum.reverse(reports)
    }
  end

  defp from_dec(decimal) do
    Decimal.to_string(decimal)
  end
end

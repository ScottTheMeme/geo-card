defmodule GeoCard.Command.ReportCard do
  @moduledoc """
  Create a report of where a card was used.
  """

  alias GeoCard.CardLoader
  alias GeoCard.Command.LocationCache
  alias GeoCard.Repo
  alias GeoCard.Repo.{Card, Report}
  import Commandex

  command do
    param :ip
    param :serial
    param :card_secret
    data :card
    data :location
    data :report

    pipeline :fetch_card
    pipeline :fetch_location
    pipeline :check_rate_limit
    pipeline :save
  end

  def fetch_card(command, %{serial: serial, card_secret: secret}, _data) do
    case CardLoader.load(serial, secret) do
      %{error: _} -> command |> put_error(:error, :not_found) |> halt()
      card -> put_data(command, :card, card)
    end
  end

  def fetch_location(command, %{ip: ip}, _data) do
    location =
      %{ip: ip}
      |> LocationCache.run()
      |> Map.get(:data)
      |> Map.get(:location)

    put_data(command, :location, location)
  end

  def check_rate_limit(command, _params, _data) do
    # TODO: Implement rate limit
    command
  end

  def save(command, _params, %{card: card, location: location}) do
    card =
      card
      |> Card.changeset(%{activated: true})
      |> Repo.update!()

    report =
      %Report{}
      |> Report.changeset(%{card_id: card.id, location_id: location.id})
      |> Repo.insert!()

    command
    |> put_data(:card, card)
    |> put_data(:report, report)
  end
end

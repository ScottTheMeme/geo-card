defmodule GeoCard.Command.LocationCache do
  @moduledoc """
  Check the stored cache in the DB for a saved hash.
  If no hash is found, GeoCard.GeoLocation will be used.
  """

  alias GeoCard.GeoLocation, as: GeoIP
  alias GeoCard.Repo
  alias GeoCard.Repo.Location
  import Commandex

  command do
    param :ip
    data :hashed_ip
    data :location

    pipeline :hash_ip
    pipeline :check_db
    pipeline :fetch_location
    pipeline :save
  end

  def hash_ip(command, %{ip: ip}, _data) do
    hashed_ip =
      :crypto.hash(:sha, ip)
      |> Base.encode64()
      |> String.slice(1..10)

    put_data(command, :hashed_ip, hashed_ip)
  end

  def check_db(command, _params, %{hashed_ip: hashed_ip}) do
    case Repo.get_by(Location, %{hashed_ip: hashed_ip}) do
      nil -> command
      location -> command |> put_data(:location, location) |> halt()
    end
  end

  def fetch_location(command, %{ip: ip}, %{location: nil}) do
    location = GeoIP.location(ip)
    put_data(command, :location, location)
  end

  def save(command, _, %{hashed_ip: hashed_ip, location: location}) do
    params = %{
      hashed_ip: hashed_ip,
      city: location.city,
      country_name: location.country_name,
      region: location.region,
      lat: location.lat,
      long: location.long
    }

    {:ok, record} =
      %Location{}
      |> Location.changeset(params)
      |> Repo.insert()

    put_data(command, :location, record)
  end
end

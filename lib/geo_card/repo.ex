defmodule GeoCard.Repo do
  use Ecto.Repo,
    otp_app: :geo_card,
    adapter: Ecto.Adapters.Postgres

  alias GeoCard.Repo
  import Ecto.Query

  def random_card do
    Repo.Card
    |> where(activated: true)
    |> order_by(fragment("RANDOM()"))
    |> limit(1)
    |> Repo.one()
    |> nil_check()
  end

  defp nil_check(nil), do: nil
  defp nil_check(%{id: id}), do: id
end

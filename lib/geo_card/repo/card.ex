defmodule GeoCard.Repo.Card do
  @moduledoc """
  Card model.
  """

  use Ecto.Schema
  import Ecto.Changeset

  schema "card" do
    field :activated, :boolean, default: false
    field :serial, :string
    field :card_secret, :string
    has_many :reports, GeoCard.Repo.Report

    timestamps()
  end

  @doc false
  def changeset(card, attrs) do
    card
    |> cast(attrs, [:activated, :serial, :card_secret])
    |> validate_required([:serial, :card_secret])
  end
end

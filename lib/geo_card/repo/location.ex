defmodule GeoCard.Repo.Location do
  @moduledoc """
  Location model.
  """

  use Ecto.Schema
  import Ecto.Changeset

  schema "location" do
    field :city, :string
    field :country_name, :string
    field :hashed_ip, :string
    field :region, :string
    field :lat, :decimal
    field :long, :decimal
    has_many :reports, GeoCard.Repo.Report

    timestamps()
  end

  @doc false
  def changeset(location, attrs) do
    location
    |> cast(attrs, [:hashed_ip, :city, :region, :country_name, :lat, :long])
    |> validate_required([:hashed_ip, :city, :region, :country_name, :lat, :long])
  end
end

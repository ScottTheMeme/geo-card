defmodule GeoCard.Repo.Report do
  @moduledoc """
  Report model.
  """

  alias GeoCard.Repo
  use Ecto.Schema
  import Ecto.Changeset

  schema "report" do
    belongs_to :card, Repo.Card
    belongs_to :location, Repo.Location

    timestamps()
  end

  @doc false
  def changeset(report, attrs) do
    report
    |> cast(attrs, [:card_id, :location_id])
    |> validate_required([])
  end
end

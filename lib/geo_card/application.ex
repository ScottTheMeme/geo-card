defmodule GeoCard.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      GeoCard.Repo,
      # Start the Telemetry supervisor
      GeoCardWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: GeoCard.PubSub},
      # Start the Endpoint (http/https)
      GeoCardWeb.Endpoint
      # Start a worker by calling: GeoCard.Worker.start_link(arg)
      # {GeoCard.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: GeoCard.Supervisor]
    sup = Supervisor.start_link(children, opts)

    GeoCard.Release.migrate()
    sup
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    GeoCardWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end

defmodule GeoCardWeb.Router do
  use GeoCardWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {GeoCardWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :admin do
    plug :browser
    plug :auth
  end

  pipeline :healthcheck do
    plug :accepts, ["json"]
  end

  scope "/healthz", GeoCardWeb do
    pipe_through :healthcheck

    get "/", PageController, :healthz
  end

  scope "/", GeoCardWeb do
    pipe_through :browser

    get "/", PageController, :index
    get "/about", PageController, :about
    get "/card/:id", PageController, :get_card
    get "/report/:serial/:card_secret", PageController, :report_card
  end

  scope "/admin", GeoCardWeb do
    pipe_through :admin

    get "/", AdminController, :index
    get "/card/:id", PageController, :get_card
  end

  defp auth(conn, _opts) do
    Plug.BasicAuth.basic_auth(conn, Application.fetch_env!(:geo_card, :basic_auth))
  end

  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser

      live_dashboard "/dashboard", metrics: GeoCardWeb.Telemetry
    end
  end

  if Mix.env() == :dev do
    scope "/dev" do
      pipe_through :browser

      forward "/mailbox", Plug.Swoosh.MailboxPreview
    end
  end
end

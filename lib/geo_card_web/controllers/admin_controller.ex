defmodule GeoCardWeb.AdminController do
  use GeoCardWeb, :controller
  alias GeoCard.Repo
  alias Repo.Card
  import Ecto.Query
  import GeoCard.Utils

  def index(conn, _params) do
    conn
    |> assign(:cards, fetch_cards())
    |> render("index.html")
  end

  def new_cards(conn, %{"form" => form}) do
    {:ok, file} = File.open("", [:ram])
    now = DateTime.utc_now()
    name = "cards-#{now.year}/#{now.month}/#{now.day}.csv"

    cards =
      for x <- 1..form["count"] do
        IO.binwrite(x)
      end

    send_download(conn, {:binary, cards}, filename: name)
    File.close(file)
  end

  def fetch_cards do
    cards =
      Card
      |> preload(:reports)
      |> Repo.all()

    for x <- cards do
      %{
        activated: x.activated,
        created: format_date(x.inserted_at),
        id: x.id,
        reports: Enum.count(x.reports),
        serial: x.serial
      }
    end
  end

  # defp export_csv(file, cards) do
  # end
end

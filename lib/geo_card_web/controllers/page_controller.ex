defmodule GeoCardWeb.PageController do
  use GeoCardWeb, :controller

  alias GeoCard.{CardLoader, Utils, Repo}
  alias GeoCard.Command.ReportCard

  def healthz(conn, _) do
    json(conn, %{
      status: "ok",
      version: to_string(Application.spec(:geo_card, :vsn))
    })
  end

  def about(conn, _params) do
    conn
    |> assign(:page_title, "About This Project")
    |> render("about.html")
  end

  def index(conn, _params) do
    card_id = Repo.random_card()

    if card_id == nil do
      render_card(conn, %{error: :no_active_cards})
    else
      redirect(conn, to: "/card/#{card_id}")
    end
  end

  def get_card(conn, %{"id" => id}) do
    id
    |> CardLoader.load()
    |> case do
      %{error: :unused} -> render_card(conn, %{error: :unused})
      %{error: :not_found} -> render_card(conn, %{error: :not_found})
      card -> render_card(conn, card)
    end
  end

  def report_card(conn, %{"serial" => serial, "card_secret" => card_secret}) do
    %{ip: Utils.fetch_ip(conn), serial: serial, card_secret: card_secret}
    |> ReportCard.run()
    |> case do
      %{success: false} -> render_card(conn, %{error: :unsuccessful})
      report -> redirect(conn, to: "/card/#{report.data.card.id}")
    end
  end

  defp render_card(conn, %{error: :no_active_cards}) do
    conn
    |> put_status(400)
    |> assign(:page_title, "No Active Cards")
    |> assign(:error, "No cards have been activated on this instance yet.")
    |> render("generic_error.html")
  end

  defp render_card(conn, %{error: _err}) do
    conn
    |> put_status(404)
    |> assign(:page_title, "Card Not Found")
    |> assign(:error, "Card not found")
    |> render("generic_error.html")
  end

  defp render_card(conn, card) do
    conn
    |> assign(:page_title, "##{card.id}")
    |> assign(:card, card)
    |> render("index.html")
  end
end

defmodule GeoCardWeb.PageControllerTest do
  use GeoCardWeb.ConnCase

  test "GET /healthz", %{conn: conn} do
    conn = get(conn, "/healthz")

    assert conn.status == 200
  end

  test "GET /", %{conn: conn} do
    new_card(1)
    conn = get(conn, "/")
    assert html_response(conn, 302) =~ "/card/"
    assert %{id: id} = redirected_params(conn)
    assert redirected_to(conn) =~ "/card/#{id}"
  end

  test "GET /about", %{conn: conn} do
    conn = get(conn, "/about")
    assert html_response(conn, 200) =~ "GeoCard"
  end

  test "GET /card/:id", %{conn: conn} do
    card = new_card(1)
    conn = get(conn, "/card/#{card.id}")
    assert html_response(conn, 200) =~ "Card ##{card.id}"
  end

  test "GET /card/:id unused", %{conn: conn} do
    card = new_card()
    conn = get(conn, "/card/#{card.id}")
    assert html_response(conn, 404) =~ "Card not found"
  end

  test "GET /card/:id not found", %{conn: conn} do
    conn = get(conn, "/card/99999999999999999")
    assert html_response(conn, 404) =~ "Card not found"
  end

  describe "GET /report/:serial/:card_secret" do
    test "valid parameters", %{conn: conn} do
      header_ip = "1.1.1.1"
      card = new_card()

      conn =
        conn
        |> Plug.Conn.put_req_header("x-client-ip", header_ip)
        |> get("/report/#{card.serial}/#{card.card_secret}")

      assert html_response(conn, 302) =~ "/card/"
      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) =~ "/card/#{id}"
    end

    test "invalid parameters", %{conn: conn} do
      header_ip = "9.9.9.9"
      serial = random_string(8)
      secret = random_string(32)

      conn =
        conn
        |> Plug.Conn.put_req_header("x-client-ip", header_ip)
        |> get("/report/#{serial}/#{secret}")

      assert html_response(conn, 404) =~ "Card not found"
    end
  end
end

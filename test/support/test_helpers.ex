defmodule GeoCard.TestHelpers do
  @moduledoc """
  Convenience functions for use in the test environment
  """

  alias GeoCard.Repo
  import GeoCard.Utils

  def new_card do
    %Repo.Card{}
    |> Repo.Card.changeset(%{
      activated: false,
      serial: random_string(8),
      card_secret: random_string(32)
    })
    |> Repo.insert!()
  end

  def new_card_activated do
    %Repo.Card{}
    |> Repo.Card.changeset(%{
      activated: true,
      serial: random_string(8),
      card_secret: random_string(32)
    })
    |> Repo.insert!()
  end

  def new_card(amount) do
    card = new_card_activated()
    example_ips = ["1.1.1.1", "8.8.8.8", "9.9.9.9"]

    locations =
      for x <- Enum.take_random(example_ips, amount) do
        GeoCard.Command.LocationCache.run(%{ip: x})
      end

    for x <- locations do
      %Repo.Report{}
      |> Repo.Report.changeset(%{card_id: card.id, location_id: x.data.location.id})
      |> Repo.insert!()
    end

    card
  end
end

defmodule GeoCard.UtilsTest do
  use GeoCardWeb.ConnCase
  import GeoCard.Utils

  @count 200
  @length 8

  test "random_string/1 unique" do
    list =
      for _ <- 1..@count do
        random_string(@length)
      end
      |> Enum.uniq()

    assert Enum.count(list) == @count
  end

  describe "fetch_ip/1" do
    test "no header" do
      conn = build_conn()
      ip = fetch_ip(conn)
      assert ip == "127.0.0.1"
    end

    test "ipv4 header" do
      header_ip = "8.8.8.8"
      conn = Plug.Conn.put_req_header(build_conn(), "x-forwarded-for", header_ip)
      ip = fetch_ip(conn)
      assert ip == header_ip
    end

    test "ipv6 header" do
      header_ip = "2606:4700:4700::1111"
      conn = Plug.Conn.put_req_header(build_conn(), "x-real-ip", header_ip)
      ip = fetch_ip(conn)
      assert ip == header_ip
    end
  end
end

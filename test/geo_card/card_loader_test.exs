defmodule GeoCard.CardLoaderTest do
  use GeoCard.DataCase
  import GeoCard.CardLoader
  import GeoCard.TestHelpers

  describe "CardLoader.load/2" do
    test "invalid serial and secret" do
      card = load("12345678", "123456789012345678901234567890")
      assert card == %{error: :not_found}
    end

    test "correct serial and invalid secret" do
      tmpcard = new_card()
      card = load(tmpcard.serial, "123456789012345678901234567890")
      assert card == %{error: :not_found}
    end

    test "invalid serial and correct secret" do
      tmpcard = new_card()
      card = load("12345678", tmpcard.card_secret)
      assert card == %{error: :not_found}
    end

    test "correct serial and secret" do
      tmpcard = new_card()
      card = load(tmpcard.serial, tmpcard.card_secret)
      refute card == %{error: :not_found}
    end
  end

  describe "CardLoader.load/1" do
    test "invalid id" do
      card = load(9_999_999_999)
      assert card == %{error: :not_found}
    end

    test "valid id" do
      tmpcard = new_card(1)
      card = load(tmpcard.id)
      refute card == %{error: :not_found}
    end
  end
end
